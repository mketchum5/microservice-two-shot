from .models import Shoe, BinVO
from common.json import ModelEncoder

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "id", "manufacturer"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",

    ]
    def get_extra_data(self, o):
        return {
            "closet_name": o.bin.closet_name,
            "bin_number": o.bin.bin_number,
            "bin_size": o.bin.bin_size
        }

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
        "bin_number",
        "bin_size",
    ]
