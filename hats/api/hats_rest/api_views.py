from .api_encoders import LocationVODetailEncoder, HatListEncoder, HatDetailEncoder
import json
from django.http import JsonResponse
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse





@require_http_methods({"GET", "POST"})
def api_list_hats(request):

    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )

    else:
        content = json.loads(request.body)

        try:
            locations = LocationVO.objects.get(import_href=content["location"])
            content["location"] = locations
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location name"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods({"GET", "DELETE", "PUT"})
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "location" in content:
                location = LocationVO.objects.get(name=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location name"},
                status=400
            )

        Hat.objects.filter(id=pk).update(**content)

        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_list_location(request):
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationVODetailEncoder)
