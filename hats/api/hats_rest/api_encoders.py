from .models import Hat, LocationVO
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number",]



class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
    "id",
    "style_name",
    "fabric",
    ]




class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "style_name", "fabric", "color", "picture_url"]

    def get_extra_data(self, o):
        return {
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number
        }
