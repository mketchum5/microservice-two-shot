import React from "react";

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            bin: '',
            bins: []};
            this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
            this.handleModelNameChange = this.handleModelNameChange.bind(this);
            this.handleColorChange = this.handleColorChange.bind(this);
            this.handleBinChange = this.handleBinChange.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            this.setState({
                manufacturer: '',
                model_name: '',
                color: '',
                bin: '',
            });

        }
    }
    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
    }
    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({model_name: value})
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }
    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value})
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/";

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        this.setState({bins: data.bins});
        };
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new shoe!</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="Manufacturer" className="form-control"/>
                                <label htmlFor="Manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleModelNameChange} value={this.state.model_name} placeholder="Model name" required type="text" name="Model name" className="form-control"/>
                                <label htmlFor="Model name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="Color" className="form-control" />
                                <label htmlFor="Color">Color</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.href}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm;
