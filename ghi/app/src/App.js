import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm'
import ShoeList from './ShoeList'
import HatForm from './HatForm';
import HatsList from './HatList';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/create" element={<HatForm />} />
          <Route path="hats/" element={<HatsList />} />
          <Route path="shoes/create" element={<ShoeForm />} />
          <Route path="shoes/" element={<ShoeList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
