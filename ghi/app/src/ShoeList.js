import React from "react";

class ShoeList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "shoes": [],
        };
        this.dlt = this.dlt.bind(this);
        this.get_fresh_data = this.get_fresh_data.bind(this);
    }
    async get_fresh_data() {
        const url = 'http://localhost:8080/api/shoes'

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({ shoes: data.shoes})
        }
    }

    async componentDidMount() {
        this.get_fresh_data()
    }
    async dlt(event) {

        const url = `http://localhost:8080/api/shoes/${event}/`
        const fetchConfig = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        await fetch(url, fetchConfig)
        this.get_fresh_data()

    }
    render() {
    return(
        <div className="container">
            <br></br>
            <a href="http://localhost:3000/shoes/create" target="_blank"><button>Add Shoes</button></a>
            <br></br>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model name</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.shoes.map(shoe => {
                        return(
                            <tr key={shoe.id}>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model_name }</td>
                                <td>
                                    <button onClick={() => this.dlt(shoe.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
}

export default ShoeList;
